# spotify-search

Project developed with VUE.
It uses the spotify api to search for songs.

## Project setup

1. Clone repository.
2. Install NodeJS.
3. In the project folder run "npm install".
4. Provide the public and private key settings of Spotify.
5. Run in the console "npm run serve".

## TO-DO
In order from most critical to least critical:
1. Separate requests from the components, there should be no logic in the components.
2. Control of errors in requests.
3. Testing.
4. Animations.
